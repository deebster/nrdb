<?php

namespace Netrunnerdb\EventsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Attendance
 */
class Attendance
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $playername;

    /**
     * @var \DateTime
     */
    private $ts;

    /**
     * @var boolean
     */
    private $validated;

    /**
     * @var integer
     */
    private $rank;

    /**
     * @var integer
     */
    private $prestige;

    /**
     * @var integer
     */
    private $corpPlayed;

    /**
     * @var integer
     */
    private $corpWon;

    /**
     * @var integer
     */
    private $runnerPlayed;

    /**
     * @var integer
     */
    private $runnerWon;

    /**
     * @var \Netrunnerdb\UserBundle\Entity\User
     */
    private $user;

    /**
     * @var \Netrunnerdb\EventsBundle\Entity\Event
     */
    private $event;

    /**
     * @var \Netrunnerdb\BuilderBundle\Entity\Decklist
     */
    private $corpDecklist;

    /**
     * @var \Netrunnerdb\CardsBundle\Entity\Card
     */
    private $corpIdentity;

    /**
     * @var \Netrunnerdb\BuilderBundle\Entity\Decklist
     */
    private $runnerDecklist;

    /**
     * @var \Netrunnerdb\CardsBundle\Entity\Card
     */
    private $runnerIdentity;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set playername
     *
     * @param string $playername
     * @return Attendance
     */
    public function setPlayername($playername)
    {
        $this->playername = $playername;

        return $this;
    }

    /**
     * Get playername
     *
     * @return string 
     */
    public function getPlayername()
    {
        return $this->playername;
    }

    /**
     * Set ts
     *
     * @param \DateTime $ts
     * @return Attendance
     */
    public function setTs($ts)
    {
        $this->ts = $ts;

        return $this;
    }

    /**
     * Get ts
     *
     * @return \DateTime 
     */
    public function getTs()
    {
        return $this->ts;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     * @return Attendance
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean 
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     * @return Attendance
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer 
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set prestige
     *
     * @param integer $prestige
     * @return Attendance
     */
    public function setPrestige($prestige)
    {
        $this->prestige = $prestige;

        return $this;
    }

    /**
     * Get prestige
     *
     * @return integer 
     */
    public function getPrestige()
    {
        return $this->prestige;
    }

    /**
     * Set corpPlayed
     *
     * @param integer $corpPlayed
     * @return Attendance
     */
    public function setCorpPlayed($corpPlayed)
    {
        $this->corpPlayed = $corpPlayed;

        return $this;
    }

    /**
     * Get corpPlayed
     *
     * @return integer 
     */
    public function getCorpPlayed()
    {
        return $this->corpPlayed;
    }

    /**
     * Set corpWon
     *
     * @param integer $corpWon
     * @return Attendance
     */
    public function setCorpWon($corpWon)
    {
        $this->corpWon = $corpWon;

        return $this;
    }

    /**
     * Get corpWon
     *
     * @return integer 
     */
    public function getCorpWon()
    {
        return $this->corpWon;
    }

    /**
     * Set runnerPlayed
     *
     * @param integer $runnerPlayed
     * @return Attendance
     */
    public function setRunnerPlayed($runnerPlayed)
    {
        $this->runnerPlayed = $runnerPlayed;

        return $this;
    }

    /**
     * Get runnerPlayed
     *
     * @return integer 
     */
    public function getRunnerPlayed()
    {
        return $this->runnerPlayed;
    }

    /**
     * Set runnerWon
     *
     * @param integer $runnerWon
     * @return Attendance
     */
    public function setRunnerWon($runnerWon)
    {
        $this->runnerWon = $runnerWon;

        return $this;
    }

    /**
     * Get runnerWon
     *
     * @return integer 
     */
    public function getRunnerWon()
    {
        return $this->runnerWon;
    }

    /**
     * Set user
     *
     * @param \Netrunnerdb\UserBundle\Entity\User $user
     * @return Attendance
     */
    public function setUser(\Netrunnerdb\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Netrunnerdb\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set event
     *
     * @param \Netrunnerdb\EventsBundle\Entity\Event $event
     * @return Attendance
     */
    public function setEvent(\Netrunnerdb\EventsBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \Netrunnerdb\EventsBundle\Entity\Event 
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set corpDecklist
     *
     * @param \Netrunnerdb\BuilderBundle\Entity\Decklist $corpDecklist
     * @return Attendance
     */
    public function setCorpDecklist(\Netrunnerdb\BuilderBundle\Entity\Decklist $corpDecklist = null)
    {
        $this->corpDecklist = $corpDecklist;

        return $this;
    }

    /**
     * Get corpDecklist
     *
     * @return \Netrunnerdb\BuilderBundle\Entity\Decklist 
     */
    public function getCorpDecklist()
    {
        return $this->corpDecklist;
    }

    /**
     * Set corpIdentity
     *
     * @param \Netrunnerdb\CardsBundle\Entity\Card $corpIdentity
     * @return Attendance
     */
    public function setCorpIdentity(\Netrunnerdb\CardsBundle\Entity\Card $corpIdentity = null)
    {
        $this->corpIdentity = $corpIdentity;

        return $this;
    }

    /**
     * Get corpIdentity
     *
     * @return \Netrunnerdb\CardsBundle\Entity\Card 
     */
    public function getCorpIdentity()
    {
        return $this->corpIdentity;
    }

    /**
     * Set runnerDecklist
     *
     * @param \Netrunnerdb\BuilderBundle\Entity\Decklist $runnerDecklist
     * @return Attendance
     */
    public function setRunnerDecklist(\Netrunnerdb\BuilderBundle\Entity\Decklist $runnerDecklist = null)
    {
        $this->runnerDecklist = $runnerDecklist;

        return $this;
    }

    /**
     * Get runnerDecklist
     *
     * @return \Netrunnerdb\BuilderBundle\Entity\Decklist 
     */
    public function getRunnerDecklist()
    {
        return $this->runnerDecklist;
    }

    /**
     * Set runnerIdentity
     *
     * @param \Netrunnerdb\CardsBundle\Entity\Card $runnerIdentity
     * @return Attendance
     */
    public function setRunnerIdentity(\Netrunnerdb\CardsBundle\Entity\Card $runnerIdentity = null)
    {
        $this->runnerIdentity = $runnerIdentity;

        return $this;
    }

    /**
     * Get runnerIdentity
     *
     * @return \Netrunnerdb\CardsBundle\Entity\Card 
     */
    public function getRunnerIdentity()
    {
        return $this->runnerIdentity;
    }
    /**
     * @var boolean
     */
    private $isValidated;

    /**
     * @var boolean
     */
    private $isDisqualified;

    /**
     * @var boolean
     */
    private $isForfeit;

    /**
     * @var boolean
     */
    private $isBye;

    /**
     * @var integer
     */
    private $sos;


    /**
     * Set isValidated
     *
     * @param boolean $isValidated
     * @return Attendance
     */
    public function setIsValidated($isValidated)
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    /**
     * Get isValidated
     *
     * @return boolean 
     */
    public function getIsValidated()
    {
        return $this->isValidated;
    }

    /**
     * Set isDisqualified
     *
     * @param boolean $isDisqualified
     * @return Attendance
     */
    public function setIsDisqualified($isDisqualified)
    {
        $this->isDisqualified = $isDisqualified;

        return $this;
    }

    /**
     * Get isDisqualified
     *
     * @return boolean 
     */
    public function getIsDisqualified()
    {
        return $this->isDisqualified;
    }

    /**
     * Set isForfeit
     *
     * @param boolean $isForfeit
     * @return Attendance
     */
    public function setIsForfeit($isForfeit)
    {
        $this->isForfeit = $isForfeit;

        return $this;
    }

    /**
     * Get isForfeit
     *
     * @return boolean 
     */
    public function getIsForfeit()
    {
        return $this->isForfeit;
    }

    /**
     * Set isBye
     *
     * @param boolean $isBye
     * @return Attendance
     */
    public function setIsBye($isBye)
    {
        $this->isBye = $isBye;

        return $this;
    }

    /**
     * Get isBye
     *
     * @return boolean 
     */
    public function getIsBye()
    {
        return $this->isBye;
    }

    /**
     * Set sos
     *
     * @param integer $sos
     * @return Attendance
     */
    public function setSos($sos)
    {
        $this->sos = $sos;

        return $this;
    }

    /**
     * Get sos
     *
     * @return integer 
     */
    public function getSos()
    {
        return $this->sos;
    }
}
